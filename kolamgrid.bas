showk
spacing% = @ask_spacing
gridtype$ = @ask_gridtype$
hidek

@start_graphics(width%, height%)

c_white = color_rgb(1, 1, 1)
color c_white

if gridtype$ = "hex"
  @draw_hex(0, 0, width%, height%, spacing%)
else if gridtype$ = "rectangular"
  @draw_rectangular(0, 0, width%, height%, spacing%)
endif

showpage
@pause_before_end
end

' #############################################

function ask_spacing()
  local spacing%
  input "Spacing? ", spacing%
  return spacing%
endfunction

function ask_gridtype$()
  local gridtype$, k$
  gridtype$ = ""

  print "Grid type? [r]ectangular or [h]hex"

  while gridtype$ = ""
    pause 0.1
    k$ = inkey$
    if k$ = "r"
      gridtype$ = "rectangular"
    endif
    if k$ = "h"
      gridtype$ = "hex"
    endif
  wend

  return gridtype$
endfunction

procedure draw_rectangular(x, y, width, height, spacing)
  local cx, cy, minx, miny, maxx, maxy

  minx = x + 5 + ((width - x - 10) mod spacing) / 2
  miny = y + 5 + ((height - y - 10) mod spacing) / 2
  maxx = x + width - 5
  maxy = y + height - 5

  cx = minx
  cy = miny

  while cy <= maxy
    pcircle cx, cy, 5
    add cx, spacing
    if cx > maxx
      cx = minx
      add cy, spacing
    endif
  wend
endprocedure

procedure draw_hex(x, y, width, height, spacing)
  local cx, cy, minx, miny, maxx, maxy, hshift%

  minx = x + 5 + ((width - x - 10) mod spacing) / 2
  miny = y + 5 + ((height - y - 10) mod (spacing * sqr(3) / 2)) / 2
  maxx = x + width - 5
  maxy = y + height - 5
  hshift% = 0

  cx = minx
  cy = miny

  while cy <= maxy
    pcircle cx, cy, 5
    add cx, spacing
    if cx > maxx
      cx = minx
      hshift% = 1 - hshift%
      if hshift%
        add cx, spacing / 2
      endif
      add cy, spacing * sqr(3) / 2
    endif
  wend
endprocedure

procedure start_graphics(var width%, var height%)
  if android?
    pause 1
  endif
  clearw
  cls
  showpage
  pause 0.1
  get_geometry ,,, width%, height%
endprocedure

procedure pause_before_end()
  ' On a PC, the graphics window closes
  ' as soon as the program ends. To allow
  ' the user to view the result, we wait
  ' for an action to indicate they are
  ' done viewing.

  ' However, on Android systems this is
  ' unnecessary, so return immediately.
  if android?
    return
  endif

  print "Press any key or click to quit."

  local wasreleased%
  wasreleased% = 0

  while 1
    pause 0.1
    exit if inkey$ <> ""
    if mousek
      exit if wasreleased%
    else
      wasreleased% = 1
    endif
  wend
endprocedure

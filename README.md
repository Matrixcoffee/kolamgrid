# Kolamgrid
A grid generator for work and art. Written in [X11Basic](http://x11-basic.sourceforge.net).

## What is this?

Need some grid paper in a pinch? Have blank paper, but no printer?

This program turns your computer or mobile screen into a [light table](https://en.wikipedia.org/wiki/Light_table) for grids. Simply place the paper over your screen, and copy the dots! Oh, and remember to set your screen's brightness to maximum.

Need a bigger grid than the size of your screen? No problem! Just shift the paper by half a screen, make sure the dots line up, and copy some more!

## Okay, but what's the point of having a grid?

Suppose you want to do something like this:

[![Colorful geometric pattern drawn around dots](612px-String_kolam.jpg)](https://commons.wikimedia.org/wiki/File:String_kolam.jpg)

or this:

[![Geometric pattern drawn by connecting dots](Vilva_ilai.png)](https://commons.wikimedia.org/wiki/File:Vilva_ilai.png)

These are called [Kolam](https://en.wikipedia.org/wiki/Kolam) / [Rangoli](https://en.wikipedia.org/wiki/Rangoli).

If you are looking for some ideas, try [Rangoliworld](https://rangoliworld.org/kolam1.html). (Or look
[here](https://www.rangoli-sans-dots.com/2013/01/simple-kolam-with-dots-collection.html),
[here](https://www.happyshappy.com/interest/kolam-rangoli),
[here](https://www.dropbox.com/s/hhgh3ohufkjzbt3/Tamil%20kolam%20designs%20book.pdf?dl=0),
or [here](http://www.kolam.net/book/patterns-and-designs)).

Or you could make your Bullet Journal pages with it. The power is yours.

## Status

Rough UI but working.

## Screenshots

Your standard rectangular grid:

![Rectangular grid, output of kolamgrid](screenshot-kolamgrid-0bea1a9a6c0224cefdbfe1567eda0985e1af0c98-2.png)

Triangular/[hexagonal](https://en.wikipedia.org/wiki/Hexagon) grid, made of [equilateral triangles](https://en.wikipedia.org/wiki/Equilateral_triangle).

![Hexagonal grid, output of kolamgrid](screenshot-kolamgrid-0bea1a9a6c0224cefdbfe1567eda0985e1af0c98-1.png)

## How to install and run

1. Download or check it out to any location you like.
2. Install [X11Basic](http://x11-basic.sourceforge.net) to your computer and/or [mobile phone](https://f-droid.org/packages/net.sourceforge.x11basic).
3. Start up the basic environment and load the file named `kolamgrid.bas`. If you have a mainstream Linux distro or compatible *NIX environment, running `make` will do this automatically. If you don't have a too new version of Android, tapping on `kolamgrid.bas` in your file manager should launch it automatically.

## Author

[Coffee](https://gitlab.com/Matrixcoffee)

## Contact
* Website: [https://open.source.coffee](https://open.source.coffee)
* Mastodon: [@Coffee@toot.cafe](https://toot.cafe/@Coffee)

## License

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

The full text of this license can be found in the file called
[LICENSE](https://gitlab.com/Matrixcoffee/kolamgrid/blob/master/LICENSE).
